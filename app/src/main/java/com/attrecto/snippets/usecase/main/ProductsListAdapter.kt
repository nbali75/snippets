package com.attrecto.snippets.usecase.main

import androidx.recyclerview.widget.DiffUtil
import com.attrecto.base.view.ViewContentListAdapter
import com.attrecto.base.view.ViewContentItemCallback
import com.attrecto.base.view.ViewContent
import com.attrecto.project.view.widget.BalanceView
import com.attrecto.project.view.widget.ProductView

class ProductListAdapter(diffUtil: DiffUtil.ItemCallback<ViewContent> = ViewContentItemCallback()) : ViewContentListAdapter(diffUtil) {

    init {
        register(ProductViewContent::class, { context -> ProductView(context) })
        register(BalanceViewContent::class, { context -> BalanceView(context) })
    }

}