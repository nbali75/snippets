package com.attrecto.snippets.usecase.main

import android.text.Spannable
import com.attrecto.base.view.ViewContent

data class ProductViewContent(val name: String, val price: Spannable, val count: Spannable, val add: (() -> Unit)?, val remove : (() -> Unit)?) : ViewContent {

    override fun areItemsTheSame(other: ViewContent): Boolean {
        return other is ProductViewContent
    }

    override fun areContentsTheSame(other: ViewContent): Boolean {
        return other is ProductViewContent && this.name == other.name
    }

}