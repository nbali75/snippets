package com.attrecto.snippets.usecase.main

import com.attrecto.base.view.ViewContent

data class BalanceViewContent(val title: String) : ViewContent {
    override fun areItemsTheSame(other: ViewContent): Boolean {
        return other is BalanceViewContent
    }

    override fun areContentsTheSame(other: ViewContent): Boolean {
        return other is BalanceViewContent && this.title == other.title
    }

}