package com.attrecto.project.view.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import com.attrecto.base.view.WidgetForViewHolder
import com.attrecto.snippets.R
import com.attrecto.snippets.databinding.ViewProductBinding
import com.attrecto.snippets.usecase.main.ProductViewContent


class ProductView(
        context: Context,
        attrs: AttributeSet? = null
) : FrameLayout(context, attrs), WidgetForViewHolder<ProductViewContent> {

    val binding: ViewProductBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_product, this, true)

    override fun setViewContent(viewContent: ProductViewContent) {
        binding.viewContent = viewContent
    }

    override val view: View
        get() = this
}