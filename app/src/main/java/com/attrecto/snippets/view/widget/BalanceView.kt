package com.attrecto.project.view.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.attrecto.base.view.WidgetForViewHolder
import com.attrecto.snippets.R
import com.attrecto.snippets.databinding.ViewBalanceBinding
import com.attrecto.snippets.usecase.main.BalanceViewContent

class BalanceView(
        context: Context,
        attrs: AttributeSet? = null
) : LinearLayout(context, attrs), WidgetForViewHolder<BalanceViewContent> {

    val binding: ViewBalanceBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_balance, this, true)

    override fun setViewContent(viewContent: BalanceViewContent) {
        binding.viewContent = viewContent
    }

    override val view: View
        get() = this
}