package com.attrecto.snippets

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.widget.ListAdapter
import android.widget.Toast
import com.attrecto.snippets.usecase.main.BalanceViewContent
import com.attrecto.snippets.usecase.main.ProductListAdapter
import com.attrecto.snippets.usecase.main.ProductViewContent
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = ProductListAdapter()
        adapter.submitList(
            listOf(
                BalanceViewContent("Hello World"),
                ProductViewContent(
                    "Hello",
                    SpannableString("test"),
                    SpannableString("product"),
                    { Toast.makeText(this, "Plus", Toast.LENGTH_SHORT).show() },
                    { Toast.makeText(this, "Minus", Toast.LENGTH_SHORT).show() }
                )
            )
        )

        this.recyclerView.adapter = adapter

    }
}
