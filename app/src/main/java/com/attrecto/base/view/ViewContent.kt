package com.attrecto.base.view

interface ViewContent {
    // TODO It is only for recyclerView
    fun areItemsTheSame(other: ViewContent): Boolean

    fun areContentsTheSame(other: ViewContent): Boolean
}