package com.attrecto.base.view

import android.content.Context
import android.util.Log
import android.util.SparseArray
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlin.reflect.KClass

open class ViewContentListAdapter(diffUtil: DiffUtil.ItemCallback<ViewContent> = ViewContentItemCallback()) :
        ListAdapter<ViewContent, ViewContentListAdapter.ViewHolder<ViewContent>>(diffUtil) {

    companion object {
        private val TAG = "DataBindingListAdapter"
    }

    var nextId = 1

    val viewTypes = HashMap<KClass<*>, Int>()
    val creators = SparseArray<(Context) -> WidgetForViewHolder<out ViewContent>>()

    override fun onBindViewHolder(holder: ViewHolder<ViewContent>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<ViewContent> {
        val viewComponent = creators[viewType]?.invoke(parent.context) as WidgetForViewHolder<ViewContent>

        viewComponent.view.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        return ViewHolder(viewComponent)
    }

    override fun getItemViewType(position: Int): Int {
        return viewTypes[getItem(position)::class] ?: super.getItemViewType(position)
    }

    fun <T : ViewContent> register(viewContent: KClass<T>, creator: (Context) -> WidgetForViewHolder<T>) {
        val id = viewTypes.get(viewContent)

        if (id != null) {
            Log.d(TAG, "Replace creator for : $viewContent")
            creators.put(id, creator)
        } else {
            Log.d(TAG, "New creator for : $viewContent")
            viewTypes.put(viewContent, nextId)
            creators.put(nextId, creator)
            nextId++
        }

    }

    class ViewHolder<T : ViewContent>(private val widgetForViewHolder: WidgetForViewHolder<T>) : RecyclerView.ViewHolder(widgetForViewHolder.view) {
        fun bind(viewContent: T) {
            widgetForViewHolder.setViewContent(viewContent)
        }
    }
}

