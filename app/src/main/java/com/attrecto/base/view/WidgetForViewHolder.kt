package com.attrecto.base.view

import android.view.View

interface WidgetForViewHolder<T : ViewContent> {
    fun setViewContent(viewContent: T)

    val view: View
}