package com.attrecto.base.view

fun <T : ViewContent> merge(item: T?, list: List<T>?): List<ViewContent?> {
    val result = mutableListOf<ViewContent?>()
    result.add(item)
    list?.let {
        result.addAll(it)
    }
    return result
}