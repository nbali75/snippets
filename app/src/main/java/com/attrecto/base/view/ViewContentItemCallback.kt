package com.attrecto.base.view

import androidx.recyclerview.widget.DiffUtil

class ViewContentItemCallback : DiffUtil.ItemCallback<ViewContent>() {
    override fun areItemsTheSame(oldItem: ViewContent, newItem: ViewContent) = oldItem.areItemsTheSame(newItem)
    override fun areContentsTheSame(oldItem: ViewContent, newItem: ViewContent) = oldItem.areContentsTheSame(newItem)
}