package com.attrecto.base.viewmodel

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.cancel

@Deprecated("Use BaseViewModel")
open class RestartableBaseViewModel(application: Application) : BaseViewModelOld(application), RestartableScope by RestartableScopeImpl() {

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onViewAppeared() {
        super.onViewAppeared()
        restart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    override fun onViewDisappeared() {
        super.onViewDisappeared()
        cancel()
    }

    override fun onCleared() {
        super.onCleared()
        cancel()
    }
}