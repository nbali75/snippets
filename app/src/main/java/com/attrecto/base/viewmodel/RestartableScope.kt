package com.attrecto.base.viewmodel

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.isActive
import kotlin.coroutines.CoroutineContext

interface RestartableScope : CoroutineScope {
    fun restart()
}

class RestartableScopeImpl : RestartableScope {

    private var scope: CoroutineScope = newScope()

    override val coroutineContext: CoroutineContext
        get() = scope.coroutineContext

    override fun restart() {
        if (!scope.isActive)
            scope = newScope()
    }

    private fun newScope() = MainScope()
}