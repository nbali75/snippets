package com.attrecto.base.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.*

@Deprecated("Use BaseViewModel")
open class BaseViewModelOld(application: Application) : AndroidViewModel(application), ApplicationResources, LifecycleObserver, LifecycleOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onViewAppeared() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onViewDisappeared() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    override val applicationContext: Context
        get() = getApplication<Application>().applicationContext

    override fun getLifecycle() = lifecycleRegistry

    fun addLifecycleObserver(observer: LifecycleObserver){
        lifecycleRegistry.addObserver(observer)
    }
}