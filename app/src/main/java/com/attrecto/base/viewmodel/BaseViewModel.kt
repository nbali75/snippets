package com.attrecto.base.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import kotlinx.coroutines.cancel

open class BaseViewModel(application: Application) : AndroidViewModel(application), ApplicationResources, LifecycleObserver, LifecycleOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    // TODO inject?
    val restartableScope = RestartableScopeImpl()

    override val applicationContext: Context
        get() = getApplication<Application>().applicationContext

    override fun getLifecycle() = lifecycleRegistry

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onViewAppeared() {
        restartableScope.restart()
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onViewDisappeared() {
        restartableScope.cancel()
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    open fun onCreate() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestroy() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    }

    fun addLifecycleObserver(observer: LifecycleObserver) {
        lifecycleRegistry.addObserver(observer)
    }

    override fun onCleared() {
        super.onCleared()
        restartableScope.cancel()
    }
}